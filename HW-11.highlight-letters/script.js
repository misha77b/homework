//                           Теоретический вопрос
// 1.Почему для работы с input не рекомендуется использовать события клавиатуры?
/* Input не дает нам корректно отслеживать ввод, ведь собитие input срабатывает при любом изменении значения, даже если оно не связанно с клавиатурными событиями.
 */

const letter = document.querySelectorAll('.btn');

window.addEventListener('keypress', function(event) {
    for (let i = 0; i < letter.length; i++) {
        if (letter[i].dataset.code === event.code) {
            letter[i].classList.add('btn-active');
            // console.log(`Your keyCode = ${event.code}`);
        } else {
            letter[i].classList.remove('btn-active');
        };
    };
});