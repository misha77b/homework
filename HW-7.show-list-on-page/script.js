//                           Теоретический вопрос
// 1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
/* DOM - это все, из чего состоит наш HTML  документ, все теги, текста, комментарии и т.д., только в DOM наши теги становятся узлами элемента, а текст становится текстовым узлом. Так же DOM позволяет скриптам получить доступ к HTMl и менять его содержимое.
 */

const array = ["Audi", "Mercedes", "BMW", "Porsche", "Bentley", "Range-Rover"];
const div = document.createElement('div');

const createList = function(array, parent = document.body) {
    const ul = document.createElement('ol');
    const listItem = array.map(item => `<li>${item}</li>`);
    ul.insertAdjacentHTML('beforeend', `${listItem.join(' ')}`)

    parent.appendChild(ul);
};

document.body.appendChild(div);
createList(array, div);
setTimeout(() => div.remove(), 5000);