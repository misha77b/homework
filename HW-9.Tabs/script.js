const tabsTitle = document.querySelector('.tabs');
const tabsTextItem = document.querySelectorAll('.tabs-text');

tabsTitle.addEventListener('click', function(event) {
    const target = event.target;
    target.closest('ul').querySelector('.active').classList.remove('active');
    target.classList.add('active');
    tabsTextItem.forEach((item) => {
        item.classList.remove('tabs-text-active')
        if (item.id === target.dataset.click) {
            item.classList.add('tabs-text-active');
        };
    })
})