//                            Теоретический вопрос
//1. Опишите своими словами как работает цикл forEach.
/* Цикл forEach используеться в тех случаях, когда нам нужно пребрать элементы массива или коллекции. Этот цикл проходит по всем элементам массива, и последовательно выполняет функцию для каждого из его элементов.
 */

const array = ['hello', 'world', 23, '23', null];
const type = 'string';
const filterBy = function(array, type) {
    return array.filter(item => {
        return typeof item !== type;
    });
};
console.log(filterBy(array, type));