const form = document.querySelector('.password-form');

form.addEventListener('click', function(event) {
    const input = event.target;

    if (input.classList.contains('fa-eye')) {
        input.previousElementSibling.setAttribute('type', 'text');
        input.classList.replace('fa-eye', 'fa-eye-slash')
    } else {
        input.previousElementSibling.setAttribute('type', 'password');
        input.classList.replace('fa-eye-slash', 'fa-eye')
    }
})

form.addEventListener('submit', function(event) {
    const inputValue1 = document.querySelector('#password').value;
    const inputValue2 = document.querySelector('#password-confirm').value;
    const wrongValueText = document.querySelector('p');

    if (wrongValueText) {
        wrongValueText.remove();
    }

    if (inputValue1 === inputValue2 && inputValue1 !== '' && inputValue2 !== '') {
        alert('You are welcome');
    } else if (inputValue1 === '' || inputValue2 === '') {
        alert('One or All of inputs are empty');
    } else if (inputValue1 !== inputValue2) {
        const wrongValues = document.createElement('p');
        wrongValues.innerText = 'Нужно ввести одинаковые значения';
        wrongValues.classList.add('text');
        form.appendChild(wrongValues);
    }
})