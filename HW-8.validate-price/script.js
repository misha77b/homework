//                         Теоретический вопрос
// 1. Опишите своими словами, как Вы понимаете, что такое обработчик событий.
/*  Обработчик событий это функция, которая откликаеться или вызываеться когда выполняеться какое-либо дейтсвие, например при клике на елемент мы вызываем функцию и запускаем выполнение действий(команд), которые в ней прописаны.
 */



const price = document.querySelector('.price');

price.addEventListener('focus', function(event) {
    price.classList.replace('price', 'price-focus');
    const errorText = document.querySelector('p')
    if (errorText) {
        errorText.remove();
    }
});


price.addEventListener('blur', function(event) {
    if (price.value === '') {
        price.classList.replace('price-focus', 'price');
    } else if (price.value < 0) {
        price.classList.replace('price-focus', 'price-wrong-focus');
        price.insertAdjacentHTML('afterend', `<p> Please, enter correct price </p>`);
    } else {
        const divContainer = document.createElement('div');
        price.classList.replace('price-wrong-focus', 'price');
        const spanPrice = document.createElement('span');
        spanPrice.innerText = `Текущая цена: ${price.value}`;

        const deleteBtn = document.createElement('button');
        deleteBtn.innerText = 'x';
        deleteBtn.id = 'deleteButton';

        divContainer.prepend(spanPrice, deleteBtn);
        document.body.prepend(divContainer);

        deleteBtn.addEventListener('click', function(event) {
            price.value = '';
            event.target.closest('div').remove();
        });
    };
});