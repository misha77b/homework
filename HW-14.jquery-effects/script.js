$(document).ready(function() {

    $('.header-link').click(function(event) {
        const href = $(this).attr('href');
        hrefTop = $(href).offset().top;
        $('html , body').animate({ scrollTop: hrefTop }, 2000);
        event.preventDeafult();
    })

    $(window).scroll(function() {
        if ($(this).scrollTop() > $('.header').height()) {
            $('#upBtn').css('visibility', 'visible');
        } else {
            $('#upBtn').css('visibility', 'hidden');
        }
    })

    $('#upBtn').click(function(event) {
        $('html , body').animate({ scrollTop: 0 }, 2000);
        event.preventDefault();
    })

    $('#newsToogle').click(function() {
        $("#hot-news").slideToggle(2000);
    })
})