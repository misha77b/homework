//                                 Теоретический вопрос
// 1.Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования.
/*Экранирование это замена уравляющих символов на escape-последовательности. В языках программирования оно используеться в тех ситуациях, когда нам НЕ надо, чтобы символ выпонял свою функцию, а просто был частью текста в строке.
 */

const firstName = prompt('Enter your name, please.');
const lastName = prompt('Enter your surname, please.');
const birthday = prompt('Enete your birthday date, please.');

const createNewUser = function(firstName, lastName, birthday) {
    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            const currentDate = new Date();
            const birthDate = birthday.split(".").reverse().join('.');
            const stringToDate = Date.parse(birthDate);
            const age = (currentDate - stringToDate) / 1000 / 3600 / 24 / 365;
            const fullAge = Math.floor(age);
            return fullAge;
        },
        getPassword() {
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4));
        }
    };

    return newUser;
};

console.log(createNewUser(firstName, lastName, birthday).getLogin());
console.log(createNewUser(firstName, lastName, birthday).getAge());
console.log(createNewUser(firstName, lastName, birthday).getPassword());