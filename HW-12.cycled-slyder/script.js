//                                        Теоретический вопрос
// 1.Опишите своими словами разницу между функциями setTimeout() и setInterval().
/* Разница в том, что setTimeout вызывает функцию один раз через заданный промежуток времени, а функция setInterval вызывает функцию регулярно.
 */

// 2. Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
/* Функция сработает так быстро, как только загрузиться страница, по причине того, что планировщик проверит эту функцию только после полного завершения загрузки кода.
 */

// 3. Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
/* По причине того, что вызванная функция setInterval будет в пустую продолжать работать, именно поэтому у нас должна быть возможность остановить ее работу.
 */

const images = document.querySelectorAll('.image-to-show');
const stopSlider = document.querySelector('#stop-slider');
const continueSlider = document.querySelector('#continue-slider');
let interval;
let currentImg = 0;

const imageSlider = function(event) {
    for (let i = 0; i < images.length; i++) {
        images[i].classList.add('hidden');
    }
    images[currentImg].classList.remove('hidden');
    currentImg++;

    if (currentImg++ == images.length) {
        currentImg = 0;
    } else {
        currentImg--;
    }
}

interval = setInterval(imageSlider, 3000);

stopSlider.addEventListener('click', function(event) {
    clearInterval(interval);
    stopSlider.disabled = true;
    continueSlider.disabled = false;
})

continueSlider.addEventListener('click', function(event) {
    interval = setInterval(imageSlider, 3000);
    continueSlider.disabled = true;
    stopSlider.disabled = false;
})