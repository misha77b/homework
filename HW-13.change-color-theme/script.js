const changeThemeBtn = document.getElementById('themeBtn');
const container = document.querySelector('.container')

window.onload = function() {
    if (localStorage.getItem('background') === 'darkslategray') {
        container.classList = 'active'
    } else {
        container.classList = 'container'
    }
}

changeThemeBtn.addEventListener('click', function() {

    if (container.classList.contains('active')) {
        container.classList.remove('active');
        localStorage.setItem('background', 'none');
    } else {
        container.classList.add('active');
        localStorage.setItem('background', 'darkslategray');
    }

})