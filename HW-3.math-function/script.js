//                        Теоретические вопросы
// 1. Описать своими словами для чего вообще нужны функции в программировании.
/* Нам нужны функции для упрощения кода, чтобы он был адаптирован ко всем сайтам. Они нам нужны для использования части кода в разных местах документа вместо того, чтобы дублировать его по много раз, или для того, чтобы мы имели возможность добавлять новые команды в определение функции, и при их вызове они выполнялись.
 */
// 2. Описать своими словами, зачем в функцию передавать аргумент.
/* Аргумент в функции нужен для того, чтобы мы могли обращаться к определенному аргументу при вызове функции, ведь если мы не будем передавать аргумент в функцию, то при ее вызове мы получим значение undefined.
 */


let argument1;
do {
    argument1 = +prompt('Enter first argument')
} while (Number.isNaN(argument1) || !argument1)

let sign;
do {
    sign = prompt("Enter operator")
} while (sign !== "+" && sign !== "-" && sign !== "*" && sign !== "/")

let argument2;
do {
    argument2 = +prompt('Enter second argument');
} while (Number.isNaN(argument2) || !argument2)

function calculate(sign, argument1, argument2) {
    let result;
    switch (sign) {
        case '+':
            result = (argument1 + argument2);
            break;
        case '-':
            result = (argument1 - argument2);
            break;
        case '/':
            result = (argument1 / argument2);
            break;
        case '*':
            result = (argument1 * argument2);
            break;
        default:
            alert('Enter one of next signs: "+" "-" "*" "/"');

    }
    return result;
}
console.log(calculate(sign, argument1, argument2));